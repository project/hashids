<?php

namespace Drupal\hashids_hash_field\Plugin\migrate\field\d7;

use Drupal\migrate_drupal\Plugin\migrate\field\FieldPluginBase;

/**
 * Plugin implementation for the 'hashids_hash' field migration.
 *
 * @MigrateField(
 *   id = "hashids_hash",
 *   type_map = {
 *     "hashids_hash" = "hashids_hash",
 *   },
 *   core = {7},
 *   source_module = "hashids_hash_field",
 *   destination_module = "hashids_hash_field",
 * )
 */
class HashIdsHasField extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getFieldFormatterMap() {
    return [
      'hashids_hash_field_formatter' => 'hashids_hash_default',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldWidgetMap() {
    return [
      'hashids_hash_field_widget' => 'hashids_hash_default',
    ];
  }

}
