<?php

namespace Drupal\hashids_hash_field\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\StringTextfieldWidget;

/**
 * Plugin implementation of the 'hashids_hash_default' widget.
 *
 * @FieldWidget(
 *   id = "hashids_hash_default",
 *   label = @Translation("Default"),
 *   field_types = {
 *     "hashids_hash"
 *   }
 * )
 */
class HashidsHashDefaultWidget extends StringTextfieldWidget {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $row = parent::formElement($items, $delta, $element, $form, $form_state);

    $row['value']['#type'] = 'hidden';

    if (empty($row['value']['#default_value'])) {
      // Force invoking the postSave method on creation.
      $row['value']['#default_value'] = '[HASH]';
    }

    return $row;
  }

}
