<?php

namespace Drupal\hashids_hash_field\Plugin\Field\FieldType;

use Drupal\Core\Field\EntityReferenceFieldItemListInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'hashids' field type.
 *
 * @FieldType(
 *   id = "hashids_hash",
 *   label = @Translation("Hashids hash"),
 *   description = @Translation("Generate Hashids hashes from other Unique Identifiers."),
 *   default_widget = "hashids_hash_default",
 *   default_formatter = "hashids_hash_default"
 * )
 */
class HashidsHashItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field) {
    return [
      'columns' => [
        'value' => [
          'type' => 'varchar',
          'length' => 256,
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['value'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('The hash value'))
      ->setSetting('case_sensitive', TRUE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function generateSampleValue(FieldDefinitionInterface $field_definition) {
    $values['value'] = \Drupal::service('hashids')->generate(mt_rand(0, 99));
    return $values;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings() {
    return [
      'host_entity' => 0,
      'related_entity' => 0,
      'reference_fields' => [],
      'update' => 1,
    ] + parent::defaultStorageSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    // Always return false to force the postSave to be called.
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function storageSettingsForm(array &$form, FormStateInterface $form_state, $has_data) {
    $entity = $this->getEntity();

    $element = [];

    $element['host_entity'] = [
      '#title' => $this->t('Host Entity'),
      '#type' => 'checkbox',
      '#default_value' => $this->getSetting('host_entity'),
      '#description' => $this->t("Use the Host Entity's ID."),
      '#disabled' => $has_data,
    ];

    $element['related_entity'] = [
      '#type' => 'checkbox',
      '#title' => $this->t("Related Entity"),
      '#default_value' => $this->getSetting('related_entity'),
      '#description' => $this->t("Use the Related Entity's ID"),
      '#disabled' => $has_data,
      '#access' => \Drupal::service('module_handler')->moduleExists('relation'),
    ];

    $reference_fields = [];
    foreach ($entity->getFieldDefinitions() as $field) {
      if ($field->getType() === 'entity_reference') {
        $name = $field->getName();
        $label = $field->getLabel();

        $reference_fields[$name] = "{$label} ({$name})";
      }
    }
    $element['reference_fields'] = [
      '#title' => $this->t('Reference Fields'),
      '#type' => 'select',
      '#multiple' => TRUE,
      '#options' => $reference_fields,
      '#default_value' => $this->getSetting('reference_fields'),
      '#description' => $this->t('Use Reference Field ID(s).'),
      '#disabled' => $has_data,
    ];

    $element['update'] = [
      '#type' => 'checkbox',
      '#title' => $this->t("Update"),
      '#default_value' => $this->getSetting('update'),
      '#description' => $this->t('Update value when updating host entity. If checked, the field value will only be populated once; when the host entity is created.'),
      '#disabled' => $has_data,
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function postSave($update) {
    if ($update && !$this->getSetting('update')) {
      return FALSE;
    }

    $entity = $this->getEntity();

    $ids = [];

    if ($this->getSetting('host_entity')) {
      $ids[] = $entity->id();
    }

    if ($this->getSetting('related_entity') && \Drupal::service('module_handler')->moduleExists('relation')) {
      $related_entity = relation_get_related_entity($entity->getEntityType(), $entity->id());
      if ($related_entity) {
        $ids[] = $related_entity->id();
      }
    }

    foreach ($this->getSetting('reference_fields') as $field_name) {
      $field = $entity->get($field_name);
      if ($field instanceof EntityReferenceFieldItemListInterface) {
        $reference_entities = $field->referencedEntities();
        foreach ($reference_entities as $reference_entity) {
          $ids[] = $reference_entity->id();
        }
      }
    }

    $hash = \Drupal::service('hashids')->generate($ids);
    $this->setValue($hash);

    return TRUE;
  }

}
