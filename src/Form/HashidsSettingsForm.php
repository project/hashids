<?php

namespace Drupal\hashids\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class HashidsSettingsForm.
 */
class HashidsSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'hashids_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['hashids.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    $config = $this->config('hashids.settings');

    $form['salt'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Salt'),
      '#description' => $this->t("Set a unique salt for your hashids."),
      '#default_value' => $config->get('salt'),
    ];

    $form['min_length'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Minimum length'),
      '#description' => $this->t("Set the minimum length for your hashids."),
      '#default_value' => $config->get('min_length'),
    ];

    $form['alphabet'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Alphabet'),
      '#description' => $this->t("Set a custom alphabet for your hashids. It must contain 16 unique characters."),
      '#default_value' => $config->get('alphabet'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('hashids.settings');

    $this->config('hashids.settings')
      ->set('salt', $form_state->getValue('salt'))
      ->set('min_length', $form_state->getValue('min_length'))
      ->set('alphabet', $form_state->getValue('alphabet'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
