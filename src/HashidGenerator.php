<?php

namespace Drupal\hashids;

use Drupal\Core\Config\ConfigFactoryInterface;
use Hashids\Hashids;

/**
 * Generates a hash id.
 */
class HashidGenerator {

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The Hashids instance.
   *
   * @var \Hashids\Hashids
   */
  protected $hashids;

  /**
   * Constructs a HashidGenerator object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $config = $config_factory->get('hashids.settings');

    $salt = $config->get('salt');
    $min_length = $config->get('min_length');
    $alphabet = $config->get('alphabet');

    if (!empty($alphabet)) {
      $this->hashids = new Hashids($salt, $min_length, $alphabet);
    }
    else {
      $this->hashids = new Hashids($salt, $min_length);
    }
  }

  /**
   * Generate a Hashids hash.
   *
   * @param mixed $numbers
   *   Numbers to encode.
   *
   * @return string
   *   The hash
   */
  public function generate(...$numbers) {
    return $this->hashids->encode(...$numbers);
  }

  /**
   * Decode a Hashids hash.
   *
   * @param string $hash
   *   The hash string to decode.
   *
   * @return array
   *   The decoded numbers.
   */
  public function decode($hash) {
    return $this->hashids->decode($hash);
  }

}
